const saludo = () => {
    alert("Sin conexion")

}

const listarProductos = () => {
    //  alert("Hola");
    /*
    codigo html para la card
    <div class="col">
                            <div class="card h-100">
                                <img src="../img/iphone.svg" width="100" height="100" class="card-img-top mt-4 mb-4" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title"><b>Precio del producto</b></h5>
                                    <p class="card-text">
                                        Nombre y Descripcion del producto
                                    </p>
                                </div>
                            </div>
                        </div>
    
    */
    let contenedorProductos = document.getElementById('contieneProductos');


    fetch(url + 'obtener/productos')
        .then(function(response) {
            return response.json();
        })
        .then(function(value) {
            //console.log(value);

            for (let index = 0; index < value.length; index++) {


                let producto = value[index]
                    //console.log(producto)
                    //produc = obtenerProductoById(producto.idProducto)
                fetch(url + 'obtener/producto/' + producto.idProducto)
                    .then(function(response) {
                        //console.log(response.json())
                        return response.json();
                    })
                    .then(function(value) {
                        console.log(value[0])
                        produc = value[0]
                        let preci = produc.precio;
                        let nom = produc.nombreProducto;
                        let desc = produc.descripcion;
                        let card = document.createElement("div")
                        card.innerHTML = `
                            <div class="col">
                                    <div class="card h-100">
                                        <img src="../img/iphone.svg" width="100" height="100" class="card-img-top mt-4 mb-4" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title"><b>` + preci + `</b></h5>
                                            <p class="card-text">
                                                ` + nom + ` y ` + desc + `
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            
                            `
                        contenedorProductos.append(card)
                        return value
                    })




            }


        });


}

const obtenerProductoById = (id) => {

    fetch(url + 'obtener/producto/' + id)
        .then(function(response) {
            //console.log(response.json())
            return response.json();
        })
        .then(function(value) {
            //  console.log(value[0])
            return value
        })

}


const load = () => {
    let input = document.getElementById('inputImage');
    console.log(input.files);
    primerArchivo = input.files[0];
    const objectURL = URL.createObjectURL(primerArchivo);
    document.getElementById('imgCargada').src = objectURL;

}

const url = '../../compraclick_back/public/';

const login = () => {
    let e = document.getElementById('staticEmail').value;
    let p = document.getElementById('inputPassword').value;
    fetch(url + 'login/autenticar?email=' + e + '&pass=' + p)
        .then(function(response) {
            return response.json();
        })
        .then(function(value) {
            if (value.estatus == 'Existe') {
                window.location = 'http://localhost/compraclick/html/micuenta.php';

            } else {
                alert('Correo electronico o contraseña incorrectos');
            }

        });
}

const registrar = () => {

    parametros = {
        "nombre": document.getElementById('nombre').value,
        "apellidoP": document.getElementById('apellidoP').value,
        "apellidoM": document.getElementById('apellidoM').value,
        "idDireccion": addDireccion(document.getElementById('direccion').value),
        "telefono": document.getElementById('telefono').value,
        "email": document.getElementById('email').value,
        "password": document.getElementById('password').value,
        "clabe": document.getElementById('clabe').value,
        "idTipoUser": document.getElementById('tipouser').value
    }

    $.ajax({
        data: parametros, //datos que se envian a traves de ajax
        url: url + 'add/usuario', //archivo que recibe la peticion
        type: 'post', //método de envio
        beforeSend: function() {
            //console.log("Procesando, espere por favor...");
            // console.log("Direccion con id: " +
            //     addDireccion(document.getElementById('direccion').value));
            //parametros['idDireccion'] = addDireccion(document.getElementById('direccion').value);
            //console.log("Direccion en: " + parametros['idDireccion']);
        },
        success: function(response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            // console.log(response);
            window.location = 'http://localhost/compraclick/html/login.html';
        }
    });
}
const vende = () => {

    parametros = {
            "idVendedor": 1,
            "nombre": document.getElementById('txtTitulV').value,
            "descripcion": document.getElementById('taDescripcion').value,
            "precio": document.getElementById('txtPrecioV').value,
            "existencias": document.getElementById('txtExistenciasV').value,
            "imagenProducto": "producto"
        }
        //let lastId;
    $.ajax({
        data: parametros,
        url: url + 'add/producto',
        type: 'post',
        beforeSend: function() {
            console.log("Procesando, espere por favor...");
        },
        success: function(response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            // var returnedData = JSON.parse(response);
            // var res = returnedData[0];
            window.location = 'http://localhost/compraclick/html/productos.php';
            // setIdDireccion(res.idDireccion);
            //lastId = res.idDireccion;
            console.log(response);

            // return res.idDireccion;
        }
    });

}

const addDireccion = (direc) => {
    descripcion = {
        'descripcion': direc
    }
    let lastId;
    $.ajax({
        async: false,
        data: descripcion,
        url: url + 'add/direccion',
        type: 'post',
        beforeSend: function() {
            console.log("Procesando, espere por favor...");
        },
        success: function(response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            var returnedData = JSON.parse(response);
            var res = returnedData[0];
            //window.location = 'http://localhost/compraclick/html/login.html';
            // setIdDireccion(res.idDireccion);
            lastId = res.idDireccion;
            //console.log(res.idDireccion);

            // return res.idDireccion;
        }
    });
    return lastId;
}
var x;
const setIdDireccion = (ide) => {
    x = ide;
}
const getIdDireccion = () => {
    return x;
}

const comprado = () => {
    alert("Comprado!");
}

const goToCompras = () => {
    console.log("A compras");
    document.getElementById("sectionCompras").classList.remove("ocultarSection");
    document.getElementById("sectionVentas").classList.add("ocultarSection");
    document.getElementById("sectionDatos").classList.add("ocultarSection");


    console.log("Hecho");

}

const goToVentas = () => {
    document.getElementById("sectionCompras").classList.add("ocultarSection");
    document.getElementById("sectionVentas").classList.remove("ocultarSection");
    document.getElementById("sectionDatos").classList.add("ocultarSection");
}

const goToDatos = () => {
    document.getElementById("sectionCompras").classList.add("ocultarSection");
    document.getElementById("sectionVentas").classList.add("ocultarSection");
    document.getElementById("sectionDatos").classList.remove("ocultarSection");
}

const listAll = () => {
    document.getElementById("sectionCompras").classList.remove("ocultarSection");
    document.getElementById("sectionVentas").classList.remove("ocultarSection");
    document.getElementById("sectionDatos").classList.remove("ocultarSection");
}