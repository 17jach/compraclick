<html>
<header>
    <nav class="navbar pt-3" style="background-color: #FFF15A; padding-bottom: 40px;">
        <div class="container d-flex justify-content-start">

            <img src="../img/icon_temp_compraclick.png" alt="compraClick" width="150" height="40">
            <form class="ms-3 d-flex justify-content-center rounded-3" style="background-color: white;">
                <input class="border-0 ms-3" type="search" placeholder="Buscar en compraClick" style="width:600px;" aria-label="Search">
                <button class="btn border-start" type="button" onclick="saludo()"><img src="../img/search_icon.png" width="15" height="15" alt="Search"></button>

            </form>
            <li class="nav-item d-flex " style="margin-left: 10px;">
                    <img src="../img/user_icon.png" style="margin-top: 15px;" width="20" height="20" alt="">
                    <a class="nav-link colorLetras mt-1" href="../html/login.html">Iniciar sesión</a>

                </li>

            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle colorLetras" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="../img/carrito_icon.png" style="margin-left: 10px; margin-top: 15px;" width="20" height="20" alt="">
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="../html/productos.php">Comprar</a></li>
                    <li><a class="dropdown-item" href="../html/vender.php">Vender</a></li>

                </ul>
            </div>
        
        </div>


    </nav>
</header>

</html>