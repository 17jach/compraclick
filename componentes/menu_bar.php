<html>
<div class="cointainer mt-3">
    <div class="row">
        <div class="col-sm-2">
            <div class="nav flex-column menu-vertical justify-content-around" style="height: 300px;">

                <a href="micuenta.php" class="nav-link letrasNegras" type="button" onclick="listAll()">
                    <img class="me-3" src="../img/menu_icon.png" width="25" height="25" alt="menu_icon"> <b> Mi cuenta</b>

                </a>

                <a class="nav-link active letrasNegras" type="button" onclick="goToCompras()" aria-current="page">
                    <img class="me-3" src="../img/shopping_bag_icon.png" width="25" height="25" alt="shopping_bag_icon"> Compras
                </a>

                <a class="nav-link letrasNegras" type="button" onclick="goToVentas()">
                    <img class="me-3" src="../img/store_icon.png" width="25" height="25" alt="store_icon"> Ventas
                </a>

                <a class="nav-link letrasNegras" type="button" onclick="goToDatos()">
                    <img class="me-3" src="../img/settings_icon.png" width="25" height="25" alt="settings_icon"> Configuración
                </a>

            </div>
        </div>
    </div>
</div>

</html>