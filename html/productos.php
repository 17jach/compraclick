<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/styles.css">

    <title>CompraClick | Comprar</title>
</head>
<style>

</style>

<?php include '../componentes/header.php'; ?>



<body onload="listarProductos()">

    <section>
        <?php include '../componentes/menu_bar.php'; ?>

    </section>

    <!-- LISTA DE PRODUCTOS -->
    <section>

    </section>

    <!-- CUANDO SELECCIONAS UN PRODUCTO-->
    <section class="ocultarSection">
        <div class="container locate-correct">
            <div class="row">
                <div class="container col-sm-12">
                    <div class="card d-flex flex-row">
                        <div class="row col-sm-3">
                            <div class="container d-flex flex-column p-4">
                                <img class="m-3" src="../img/slide_compu1.jpeg" width="50" height="50" alt="">
                                <img class="m-3" src="../img/slide_compu1.jpeg" width="50" height="50" alt="">
                                <img class="m-3" src="../img/slide_compu1.jpeg" width="50" height="50" alt="">
                            </div>
                        </div>
                        <div class="row col-sm-6">
                            <div class="container">
                                <img src="../img/slide_compu1.png" width="350" height="350" alt="">
                            </div>
                        </div>
                        <div class="row col-sm-3 card mt-3 mb-3">
                            <div class="card-header">
                                Nuevo | 297 Vendidos
                            </div>
                            <div class="card-body ">
                                <h5 class="card-title">Nombre del producto</h5>
                                <p class="card-text">PrecioProducto</p>
                                <p class="card-text">Disponibilidad</p>
                                <p class="card-text">Cuantos disponibles</p>

                                <div class="d-grid gap-2 col-8 mx-auto">
                                    <a class="btn btn-primary" type="button" onclick="comprado()">Comprar ahora</a>
                                    <a href="../html/carrito.html" class="btn btn-outline-primary border-0" type="button">Agregar al carrito</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="locate-correct">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="contieneProductos" class="row row-cols-1 row-cols-md-5 g-4">
                        

                    </div>
                </div>
            </div>
        </div>

    </section>

    <script src="../js/app.js"></script>
    <script src="../js/pooper.js"></script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>