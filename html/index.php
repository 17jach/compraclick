<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/styles.css">

    <title>CompraClick | Home</title>
</head>

<style>

</style>

<?php include '../componentes/header.php'; ?>

<body>
    <section>
        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="10000">
                    <img src="../img/slide_prueba.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-flex justify-content-start flex-wrap">
                        <h1 class="letrasBlancas" style="font-size: 100%;">PRODUCTOS NACIONALES <br></h1>
                        <p class="letrasBlancas" style="font-size: 350%;"><b>Hechos de cultura y tradición</b> </p>

                        <h2 class="letrasBlancas border-start ps-2"> <b>ENVIOS EN 24 HRS</b> <br> Gratis desde 299$</h2>
                    </div>
                </div>
                <div class="carousel-item " data-bs-interval="2000">
                    <img src="../img/slide_prueba.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-flex justify-content-start flex-wrap">
                        <h1 class="letrasBlancas" style="font-size: 100%;">PRODUCTOS NACIONALES <br></h1>
                        <p class="letrasBlancas" style="font-size: 350%;"><b>Hechos de cultura y tradición</b> </p>

                        <h2 class="letrasBlancas border-start ps-2"> <b>ENVIOS EN 24 HRS</b> <br> Gratis desde 299$</h2>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/slide_prueba.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-flex justify-content-start flex-wrap">
                        <h1 class="letrasBlancas" style="font-size: 100%;">PRODUCTOS NACIONALES <br></h1>
                        <p class="letrasBlancas" style="font-size: 350%;"><b>Hechos de cultura y tradición</b> </p>

                        <h2 class="letrasBlancas border-start ps-2"> <b>ENVIOS EN 24 HRS</b> <br> Gratis desde 299$</h2>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

    </section>

    <section id="sectionCardsFoot">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="card col-3">
                            <div class="card-body ">
                                <h5 class="card-title">Paga cómodo y seguro</h5>
                                <p class="card-text">Sólo en tiendas participantes.</p>
                            </div>
                        </div>

                        <div class="row card col-6 d-flex justify-content-center">

                            <div class="card-body">
                                <h5 class="card-title">Hasta tres meses sin intereses con:</h5>
                                <p class="card-text">
                                    <a href="#">Ver condiciones</a>
                                </p>

                            </div>

                            <img src="../img/citibanamexLogo.png" style="width: 30%; margin-top: -80px;
                            margin-left: 380px;" class="card-img-top" alt="...">
                        </div>

                        <div class="card col-3">

                            <div class="card-body d-flex">
                                <img src="../img/btn_plus_icon.png" style="margin-top: 15px;" width="20" height="20" alt="">
                                <div class="container">

                                    <h class="card-title">Más medios de pago</h>
                                    <p class="card-text">
                                        <a href="#">Ver todos</a>
                                    </p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="../js/app.js"></script>
    <script src="../js/pooper.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>