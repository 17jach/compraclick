<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/styles.css">

    <title>CompraClick | Vender</title>
</head>
<style>

</style>
<?php include '../componentes/header.php'; ?>




<body>


    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 d-flex justify-content-center">
                    <div class="col-4" style="margin-top: -30px;">

                        <div class="card" style="width: 18rem;">

                            <!--Este debe de ser un input file con el diseño que se ve-->
                            <!-- <img src="../img/subir_imagen.jpg" width="40" class="card-img-top" alt="...">
                            <p class="text-center">Subir imagen</p>
                             -->
                            <div class="mb-3">
                                <!-- <label for="formFile" class="form-label">Default file input example</label> -->
                                <label for="inputImage">
                                    <img id="imgCargada" src="../img/subir_imagen.jpg" width="40" class="card-img-top" alt="..."></label>
                                <input style="display: none;" onchange="load()" type="file" id="inputImage">
                            </div>

                            <div class="card-body">
                                <div class="d-flex flex-column g-3">
                                    <div class="col-sm-12 mb-3">
                                        <input id="txtTitulV" type="text" class="form-control" placeholder="Titulo" aria-label="Titulo">
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <input id="txtPrecioV" type="text" class="form-control" placeholder="Precio" aria-label="Precio">
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <input id="txtExistenciasV" type="text" class="form-control" placeholder="Cantidad de ejemplares" aria-label="Cantidad de ejemplares">
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <!--Este se debe cambiar por un textarea-->
                                        
                                        <textarea name="descripcion" id="taDescripcion" placeholder="Desccripcion" class="form-control" ></textarea>
                                    </div>
                                    <div class="col-sm-12 mb-3 d-flex justify-content-center">
                                        <a class="btn btn-primary" onclick="vende()" type="submit" >Publicar producto</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="../js/app.js"></script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/pooper.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>