<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/styles.css">

    <title>CompraClick | Vender</title>
</head>
<style>

</style>
<?php include '../componentes/header.php'; ?>



<body>

    <div class="container d-flex justify-content-center">
        <div class="row col-sm-6 mt-5">
            <div class="col-8 ">
                <h5 class="row" style="font-size: 35px;">¡Publicado con éxito!</h5>
                <div class="row d-flex justify-content-between mt-4">
                    <div class="col ">
                        <a href="../html/productos.php" class="btn btn-primary">Ir al inicio</a>
                    </div>
                    <div class="col ">
                        <!--Pero se supone que llevará algunos parametros para que te muetre el producto-->
                        <a href="../html/productos.php" class="btn btn-primary">Ver publicación</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <img src="../img/checked.svg" width="120" alt="">
            </div>
        </div>

    </div>

    <script src="../js/app.js"></script>
    <script src="../js/pooper.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>